$ErrorActionPreference = 'Stop'

function CreateShortcut {
    param([String] $Path)

    $shortcutArgs = @{
        shortcutFilePath = "$Path\Jellyfin.lnk"
        targetPath       = 'http://localhost:8096'
        iconLocation     = "$ENV:ProgramFiles\Jellyfin\Server\jellyfin-web\favicon.ico"
    }

    Install-ChocolateyShortcut @shortcutArgs
}

$v = "10.8.5"

$packageArgs = @{
    packageName    = 'jellyfin'
    fileType       = 'EXE'
    url64bit       = "https://repo.jellyfin.org/releases/server/windows/versions/stable/installer/${v}/jellyfin_${v}_windows-x64.exe"
    checksum64     = 'b6f12369b69959752725fe1c717009a125377e14621409dce144cfd30991d401'
    checksumType64 = 'sha256'
    silentArgs     = '/S'
    validExitCodes = @(0,1)
    softwareName   = 'Jellyfin Server*'
}

Remove-Process jellyfin
Install-ChocolateyPackage @packageArgs

CreateShortcut -Path "$ENV:Public\Desktop"
CreateShortcut -Path "$ENV:ProgramData\Microsoft\Windows\Start Menu\Programs"
